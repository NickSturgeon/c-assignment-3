/* PROGRAM:             players.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/12/03
   PURPOSE:             Initialize players for card game
   LEVEL OF DIFFICULTY: 1
   CHALLENGES:          None
   HOURS SPENT:         5 minutes
*/

#include "deck.h"
#include <stdio.h>
#include <stdlib.h>

/* Get number of players and the cards per hand */
int get_players(int *num_players, int *num_cards, int arg_players, int arg_cards) {
	if (arg_players < 2 || arg_players > 4) {
	  printf("Invalid number of players (between 2 and 4)\n");
    return EXIT_FAILURE;
	} else {
	  *num_players = arg_players;
	}

	if (52 / *num_players < arg_cards || arg_cards <= 0) {
	  printf("Invalid number of cards (between 1 and %d)\n", 52 / *num_players);
    return 1;
	} else {
	  *num_cards = arg_cards;
	}
  
  return EXIT_SUCCESS;
}
