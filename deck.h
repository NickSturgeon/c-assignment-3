#ifndef DECK_H
#define DECK_H

enum suit {
  Spades,
  Hearts,
  Clubs,
  Diamonds
};

struct card {
  int rank;
  enum suit suit;
  char *(*name)();
  int (*points)();
};

void initialize_deck(void);
int points(struct card *);
char *name(struct card *);
void deal(int, int);
int show_hands(int, int, char *);

#endif