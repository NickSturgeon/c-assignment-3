/* PROGRAM:             deck.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/12/03
   PURPOSE:             Functions relating to a card deck
   LEVEL OF DIFFICULTY: 3
   CHALLENGES:          Structs and function pointers
   HOURS SPENT:         3 hours
*/

#include "deck.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define NAME_LENGTH 20

#define A 1
#define J 11
#define Q 12
#define K 13

struct card deck[4][13];
struct card *hands[4][13];
int dealt[4][13];

/* Initialize the card structs in the deck array */
void initialize_deck(void) {
  enum suit suit;
  int rank;
  
  for (suit = Spades; suit <= Diamonds; suit++) {
    for (rank = 0; rank < 13; rank++) {
      struct card new_card;
      new_card.rank = rank;
      new_card.suit = suit;
      new_card.name = name;
      new_card.points = points;
      deck[suit][rank] = new_card;
    }
  }
}

/* Return the string name of the card */
char *name(struct card *self) {
  char *name = malloc(NAME_LENGTH);
  
  switch (self->rank + 1) {
    case  A: strcpy(name, "Ace");   break;
    case  2: strcpy(name, "Two");   break;
    case  3: strcpy(name, "Three"); break;
    case  4: strcpy(name, "Four");  break;
    case  5: strcpy(name, "Five");  break;
    case  6: strcpy(name, "Six");   break;
    case  7: strcpy(name, "Seven"); break;
    case  8: strcpy(name, "Eight"); break;
    case  9: strcpy(name, "Nine");  break;
    case 10: strcpy(name, "Ten");   break;
    case  J: strcpy(name, "Jack");  break;
    case  Q: strcpy(name, "Queen"); break;
    case  K: strcpy(name, "King");  break;
    default: strcpy(name, "Unknown");
  }
  
  switch (self->suit) {
    case Spades:   strcat(name, " of Spades");   break;
    case Hearts:   strcat(name, " of Hearts");   break;
    case Clubs:    strcat(name, " of Clubs");    break;
    case Diamonds: strcat(name, " of Diamonds"); break;
    default:       strcat(name, " of Unknown");
  }
  
  return name;
}

/* Return the point value of the card */
int points(struct card *self) {
  switch (self->rank + 1) {
    case  A: return 4;
    case  K: return 3;
    case  Q: return 2;
    default: return 1;
  }
}

/* Deal the cards in random order to the players' hands */
void deal(int num_players, int num_cards) {
  int suit, rank, chosen;
  int i, j;
	srand(time(NULL));
	for (i = 0; i < num_players; i++) {
		for (j = 0; j < num_cards; j++) {
			chosen = 0;
			do {
				suit = rand() % 4;
				rank = rand() % 13;
				if (!dealt[suit][rank]) {
					hands[i][j] = &deck[suit][rank];
				  dealt[suit][rank] = 1;
					chosen = 1;
				}
			} while (!chosen);
		}
	}
}

/* Display the hands by writing to a file */
int show_hands(int num_players, int num_cards, char *output_file) {
  FILE *output;
  int total_points;
  int i, j;
  
	output = fopen(output_file, "w+b");
	if (output == NULL) {
		printf("Error opening output file '%s'\n", output_file);
    return EXIT_FAILURE;
	} else {
		printf("Output file '%s' opened successfully\n", output_file);
	}
  
  for (i = 0; i < num_players; i++) {
    total_points = 0;
    fprintf(output, "Hand #%d\n----------\n", i + 1);
    for (j = 0; j < num_cards; j++) {
      total_points += hands[i][j]->points(hands[i][j]);
      fprintf(output, "%-20s: %d\n", hands[i][j]->name(hands[i][j]), hands[i][j]->points(hands[i][j]));
    }
    fprintf(output, "----------\nTotal Points        : %d", total_points);
    fprintf(output, "\n\n");
  }
  
  fclose(output);
  return EXIT_SUCCESS;
}

