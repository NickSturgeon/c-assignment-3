CC = gcc-8
CC_FLAGS = -g -ansi -pedantic -Wall
FILES = main.c deck.c players.c
OUT = assignment3

build: $(FILES)
	$(CC) $(CC_FLAGS) -o $(OUT) $(FILES)

clean: 
	rm -rf *.o core assignment3 *~ *.dSYM

rebuild: clean build
