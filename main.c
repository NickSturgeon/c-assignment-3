/* PROGRAM:             main.c
   AUTHOR:              Nicholas Sturgeon
   DATE:                2018/12/03
   PURPOSE:             Program entry point
   LEVEL OF DIFFICULTY: 1
   CHALLENGES:          None
   HOURS SPENT:         5 minutes
*/

#include "deck.h"
#include "players.h"
#include <stdio.h>
#include <stdlib.h>

int num_players, num_cards;

/* Main entry point */
int main (int argc, char * argv[]) {
  if (argc != 4) {
    printf("Invalid number of arguments\n");
    return EXIT_FAILURE;
  }
  
  if (get_players(&num_players, &num_cards, atoi(argv[2]), atoi(argv[3])))
    return EXIT_FAILURE;
  
  initialize_deck();
  deal(num_players, num_cards);
  
  if(show_hands(num_players, num_cards, argv[1]))
    return EXIT_FAILURE;

  
  return EXIT_SUCCESS;
}
